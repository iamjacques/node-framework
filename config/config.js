// config.js
const env = process.env.NODE_ENV; // 'dev' or 'prod'

const dev = {
    app: {
        port: parseInt(process.env.DEV_APP_PORT) || 8080
     },
     db: {
        host: process.env.DEV_DB_HOST || 'localhost',
        port: parseInt(process.env.DEV_DB_PORT) || 27017,
        name: process.env.DEV_DB_NAME || 'test'
     },
     cors: {
         
     }
};

const prod = {
 app: {
    port: parseInt(process.env.PROD_APP_PORT) || 8080
 },
 db: {
    host: process.env.DEV_DB_HOST || 'localhost',
    port: parseInt(process.env.DEV_DB_PORT) || 27017,
    name: process.env.DEV_DB_NAME || 'test'
 },
 cors: {
     
 }
};

const config = {
    dev,
    prod
};

module.exports = config.dev;