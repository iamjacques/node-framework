
const express = require('express');
const http = require('http');
const fs = require('fs');
const cors = require('cors');
const helmet = require('helmet');
const compression = require('compression');
const methodOverride = require( 'method-override');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const appRoot = require('app-root-path').path;

const errorhandler = require('errorhandler');
const logger = require('morgan')
const session = require('express-session');
const { app: { port } } = require('../config/config');
module.exports = class App {
    
    constructor() {
        this.init();
    }

    /**
     * Initialise app
     */
    init(){
        // Express initialization
        const app = express();

        // Initialise middleware
        app.use(cors());
        app.use(helmet());
        app.use(compression());
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(bodyParser.json());
        app.use(methodOverride());
        app.use(cookieParser());
        app.use(logger('dev'));
        //require('../routes/routes')()
        app.use(require('../routes/routes'));

        // Initialise providers 
         require('./providers').map((provider) => {
            require(`${appRoot}/${provider}`).register()
         });
        
         // Create server
        this.app = http.createServer(app);
    }
    
    /**
     * Start the server
     */
    start(){
        this.app.listen(port, (err) => {
            if (err) {
                console.log('Failed to listen on port ' + err);
                process.exit(1);
            }
            console.log('Started listening on port ' + port);
        })
    }
}