const EventEmitter = require('events')
const uuid = require('uuid');
const channels = require('../../database/channels.seed');


/**
* Represents a Channel Handler.
* @constructor
*/
class ChannelHandler  extends EventEmitter{

    constructor(channels) {
        super();
        this.subscriber = [
            {
                subscriber_id: 1,
                subscriber_name: 'Jacques Uwamungu',
                subscribed_to: [
                    'e2d5cb2f-661a-40fa-876e-2f4340e3977f'
                ]
            },
            {
                subscriber_id: 2,
                subscriber_name: 'Naomi Uwamungu',
                subscribed_to: [
                    'e2d5cb2f-661a-40fa-876e-2f4340e3977f'
                ]
            }
        ]

        console.log(channels);

        //this.channels = channels;
    }

    /**
     * Broadcast events to the channel
     * @param {string} channel_id 
     * @param {object} message 
     */
    publish(channel_id, message) {
        this.channels.filter(channel => {
            if(channel.channel_id === channel_id){
                this.emit(channel_id, message);
            }
        })
    };


    /**
     * Add subscriber to a channel
     * @param {string} channel_id 
     * @param {string} subscriber_id 
     */
    subscribe(channel_id, subscriber_id){
       
    }


    /**
     * Remove a subscriber from a channel
     * @param {string} channel_id 
     * @param {string} subscriber_id 
     */
    unsubscribe(channel_id, subscriber_id){

    }


    /**
     * Create channels
     * Expects Array of object(s)
     * @param {array} channels 
     */
    addChannel(channels){
        channels.map(channel => channel.channel_id = uuid.v4())
        this.channels = [...channels, ...this.channels];
    }
    

    /**
     * Get channel by id
     * @param { string } channel_id
     * @return { object }
     */
    channel(channel_id){
        if(typeof channel_id !== undefined){
            return this.channels.find(chan => chan.channel_id === channel_id) || ({});   
        }
        return {};
    }


    /**
     * 
     */
    subscribers(channel_id){}
}
module.exports = ChannelHandler;