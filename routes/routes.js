
var express = require('express');
var routes = express.Router();
var User = require('../app/models/user');
const uuid = require('uuid');

routes.get('/user/:id', function (req, res) {
  User.findById(req.params.id, function(err, user) {
    if (err) throw err;
    res.send(user);
  });
})
  
routes.get('/users', function (req, res) {
  User.find({}, function(err, users) {
    if (err) throw err;
    res.send(users);
  });
})

routes.post('/user', function (req, res) {
  var user = new User({
    name: 'Jacques',
    email:  uuid.v4(),
    username:  uuid.v4(),
    password: 'password' 
  });
  
  user.save(function(err) {
    if (err) throw err;
    res.send(user);
  });
})

 module.exports = routes;