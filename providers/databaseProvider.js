
const mongoose = require('mongoose');
const { db: { host, port, name } } = require('../config/config');

module.exports.register = () => {
    mongoose.connect(`mongodb://${host}:${port}/${name}`, { useNewUrlParser: true });
    console.log(`Initialised DB provider`);
}